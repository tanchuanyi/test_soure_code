#include <linux/notifier.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/fs.h>

#define TESTCHAIN_INIT 0x52U
static RAW_NOTIFIER_HEAD(test_chain);
extern int register_test_notifier(struct notifier_block *nb);


int  test_init_evevnt(struct notifier_block *nb, unsigned long event, void *v)
{
     switch( event )
     {
        case TESTCHAIN_INIT:
	   printk (KERN_DEBUG"I got the chain event: test_chain_2 is on the way of init\n");
           break;
        default:
           break;
     }
     return NOTIFY_DONE;

}

struct notifier_block  test_init_notifier = {
       .notifier_call = test_init_evevnt,

};


static  int __init test_chain_1_init(void)
{
       printk(KERN_DEBUG "I'm in test chain 1\n");
       register_test_notifier(&test_init_notifier);
       return 0;
}

static  void __exit test_chain_1_exit(void)
{
       printk(KERN_DEBUG "byebye test chain 1\n");

}


module_init(test_chain_1_init);
module_exit(test_chain_1_exit);
