#include <linux/notifier.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/fs.h>

#define TESTCHAIN_INIT 0x52U
static RAW_NOTIFIER_HEAD(test_chain);

static call_test_notifiers(unsigned long val, void *v )
{
   return raw_notifier_call_chain(&test_chain ,val,v);
}
EXPORT_SYMBOL(call_test_notifiers);

static int register_test_notifier(struct notifier_block *nb)
{
    int err;
    err = raw_notifier_chain_register(&test_chain, nb);
    if(err)
         goto out;
out:
    return err;
}
EXPORT_SYMBOL(register_test_notifier);

static  int __init test_chain_0_init(void)
{
       printk(KERN_DEBUG "I'm in test chain 0\n");
       return 0;
}

static  void __exit test_chain_0_exit(void)
{
       printk(KERN_DEBUG "byebye test chain 0\n");

}


module_init(test_chain_0_init);
module_exit(test_chain_0_exit);
