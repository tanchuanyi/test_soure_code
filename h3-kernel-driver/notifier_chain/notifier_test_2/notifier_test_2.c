#include <linux/notifier.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/fs.h>

#define TESTCHAIN_INIT 0x52U
extern int call_test_notifiers(unsigned long val, void *v );

static  int __init test_chain_2_init(void)
{
       printk(KERN_DEBUG "I'm in test chain 2\n");
       call_test_notifiers(TESTCHAIN_INIT, "no_use");
       return 0;
}

static  void __exit test_chain_2_exit(void)
{
       printk(KERN_DEBUG "byebye test chain 2\n");

}


module_init(test_chain_2_init);
module_exit(test_chain_2_exit);
